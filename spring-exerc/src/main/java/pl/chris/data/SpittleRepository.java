package pl.chris.data;

import java.util.List;

import pl.chris.model.Spittle;

public interface SpittleRepository {

	List<Spittle> findRecentSpittles();

	List<Spittle> findSpittles(long max, int count);

	Spittle findOne(long id);

	void save(Spittle spittle);

}