package pl.chris.data;

import pl.chris.model.Spitter;

public interface SpitterRepository {

	Spitter save(Spitter spitter);

	Spitter findByUsername(String username);
}
